/*
* Interface name: Calculator.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.Calculator;

public interface Calculator {
	
	public void calculate(int decimalDegree);
	
	public void printResult();
	
	public void terminate();

}
