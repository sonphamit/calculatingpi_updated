/*
* Classname: BigCalculator.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.Calculator;

import java.math.BigDecimal;
import com.sonpham.it.CalculatingPi.CalculatingThread.BigCalculatingThread;
import com.sonpham.it.CalculatingPi.Common.Constant;
import com.sonpham.it.CalculatingPi.SupportThread.ListenerThread;

/*
 * The class is used to calculate pi with small decimal place which is greater than or equals 17.
 * */
public class BigCalculator implements Calculator {
	
	private BigCalculator(){}
	private static class SingletonHelper{
		private static final BigCalculator instance = new BigCalculator();
	}

	public static BigCalculator getInstance(){
		return SingletonHelper.instance;
	}
	
	BigDecimal resultPi = new BigDecimal(0);
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#calculate(int)
	 */
	public void calculate(int decimalDegree) {
		
		long fromIndex = 0;
		long toIndex = Constant.MAX_VALUE_PER_THREAD;
		
		Thread [] threads = new Thread [Constant.MAX_THREAD];
	
		BigCalculatingThread [] bigCalculatingThreads = new BigCalculatingThread [Constant.MAX_THREAD];
		
		for (int i = 0; i < Constant.MAX_THREAD; i++){
			
			bigCalculatingThreads[i] = new BigCalculatingThread(fromIndex,toIndex,decimalDegree);
			threads[i] = new Thread(bigCalculatingThreads[i]);
			
			threads[i].start();
		
			/* increase value of fromIndex and toIndex for the next thread.*/
			fromIndex = toIndex;
			
			toIndex = toIndex + Constant.MAX_VALUE_PER_THREAD;
		}
		
		ListenerThread listener = new ListenerThread();
		listener.terminate();
		
		// stop all thread.
		if(!listener.isRunning()) {
			for (int i = 0; i < Constant.MAX_THREAD; i++) {
				bigCalculatingThreads[i].shutdown();
				resultPi = resultPi.add(bigCalculatingThreads[i].getResult());
			}
		}
		else {
			for (int i = 0; i < Constant.MAX_THREAD; i++){
				try {
					threads[i].join();
					resultPi = resultPi.add(bigCalculatingThreads[i].getResult());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		resultPi = resultPi.multiply(Constant.FOUR);
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#printResult()
	 */
	public void printResult() {
		System.out.print("\n\n\tPI = " + resultPi + "\n");
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#terminate()
	 */
	public void terminate() {
		
	}
	

}
