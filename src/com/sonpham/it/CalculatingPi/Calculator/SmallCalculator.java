/*
* Classname: SmallCalculator.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.Calculator;

import com.sonpham.it.CalculatingPi.CalculatingThread.SmallCalculatingThread;
import com.sonpham.it.CalculatingPi.Common.Constant;
import com.sonpham.it.CalculatingPi.SupportThread.ListenerThread;
/*
 * The class is used to calculate pi with small decimal place from 1 to 16.
 * */
public class SmallCalculator implements Calculator {
	
	private SmallCalculator(){}
	
	private static class SingletonHelper{
		private static final SmallCalculator instance = new SmallCalculator();
	}
	
	public static SmallCalculator getInstance(){
		return SingletonHelper.instance;
	}
	
	private double resultPi = 0;

	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#calculate(int)
	 */
	public void calculate(int decimalDegree) {
		
		long fromIndex = 0; // index of starting a term.
		long toIndex = Constant.MAX_VALUE_PER_THREAD; // index of ending a term.
		Thread [] threads = new Thread [Constant.MAX_THREAD];
		SmallCalculatingThread [] smallCalculatingThreads = new SmallCalculatingThread[Constant.MAX_THREAD];
		
		for (int i = 0; i < Constant.MAX_THREAD; i++){
			smallCalculatingThreads[i] = new SmallCalculatingThread(fromIndex,toIndex,decimalDegree);
			threads[i] = new Thread(smallCalculatingThreads[i]);
			threads[i].start();
			
			/* increase value of fromIndex and toIndex for the next thread.*/
			fromIndex = toIndex;
			toIndex = toIndex + Constant.MAX_VALUE_PER_THREAD;
		}
		ListenerThread listener = new ListenerThread();
		listener.terminate();
		// stop all thread.
		if(!listener.isRunning()) {
			for (int i = 0; i < Constant.MAX_THREAD; i++) {
				smallCalculatingThreads[i].shutdown();
				resultPi += smallCalculatingThreads[i].getResult();
			}
		}
		else {
			for (int i = 0; i < Constant.MAX_THREAD; i++){
				try {
					threads[i].join();
					resultPi += smallCalculatingThreads[i].getResult();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
		resultPi = Math.round(resultPi * 4 * Math.pow(10, decimalDegree)) / Math.pow(10, decimalDegree);				
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#printResult()
	 */
	public void printResult(){
		 System.out.print("\n\n\tPI = " + resultPi + "\n");
	}
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.Calculator.Calculator#terminate()
	 */
	public void terminate(){
		
	}
}
