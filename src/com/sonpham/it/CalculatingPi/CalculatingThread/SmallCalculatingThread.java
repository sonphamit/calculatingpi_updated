/*
* Classname: SmallCalculatingThread.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.CalculatingThread;

/*
 *The class is used to run and calculate result of each threads.
*/
public class SmallCalculatingThread extends CalculatingThread<Double> implements Runnable{
	
	private int decimalDegree; 
	private volatile double result;
	private boolean running = true;
	
	/**
	 * @param fromIndex Threads are started by fromIndex.
	 * @param toIndex  Threads finish by toIndex.
	 * @param decimalDegree The decimal place which users need.
	 */
	public SmallCalculatingThread(long fromIndex, long toIndex, int decimalDegree) {
		super(fromIndex,toIndex);
		this.decimalDegree = decimalDegree;
	}

	@Override	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		for (long n = fromIndex; n < toIndex && running; n++){
			
			// The terms are found by using by condition.
			if (n <= 5 * Math.pow(10,decimalDegree -1) - 1 / 2){
				result = result + (double)Math.pow(-1, n) / (2 * n + 1);
			}
		}
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.CalculatingThread.CalculatingThread#getResult()
	 */
	public Double getResult() {
		return result;
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.CalculatingThread.CalculatingThread#shutdown()
	 */
	public void shutdown(){
		running = false;
	}

}
