/*
* Classname: BigCalculatingThread.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.CalculatingThread;

import java.math.BigDecimal;

import com.sonpham.it.CalculatingPi.Common.Constant;

public class BigCalculatingThread extends CalculatingThread<BigDecimal> implements Runnable{

	private int decimalDegree;
	private boolean running = true;
	
	private volatile BigDecimal result = new BigDecimal(0);
	/** 
	 * @param fromIndex Threads are started by fromIndex.
	 * @param toIndex  Threads finish by toIndex.
	 * @param decimalDegree The decimal place which users need.
	 */
	public BigCalculatingThread(long fromIndex, long toIndex, int decimalDegree) {
		super(fromIndex, toIndex);
		this.decimalDegree = decimalDegree;
	}

	@Override
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {	
		BigDecimal i  = Constant.ONE;
		for (long n = fromIndex; n < toIndex && running; n++){
			
			// The terms are found by using by condition.
			if (n <= 5 * Math.pow(10, decimalDegree -1) -1 / 2){
				// change type of n
				BigDecimal bigDecimalN = new BigDecimal(n);
				// calculate result
				result = result.add(i.divide((Constant.TWO.multiply(bigDecimalN).add(Constant.ONE)), decimalDegree, BigDecimal.ROUND_HALF_UP));
				i = i.multiply(Constant.N_ONE);
			}
		}
		
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.CalculatingThread.CalculatingThread#getResult()
	 */
	public BigDecimal getResult() {
		
		return result;
	}
	
	@Override
	/* (non-Javadoc)
	 * @see com.sonpham.it.CalculatingPi.CalculatingThread.CalculatingThread#shutdown()
	 */
	public void shutdown() {
		running = false;
	}

}
