/*
* Classname: CalculatingThread.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.CalculatingThread;

public abstract class CalculatingThread <T> {
	
    long fromIndex;
    long toIndex;
    boolean running;
    /**
     * @param fromIndex Threads are started by fromIndex.
     * @param toIndex Threads finish by toIndex.
     */
    public CalculatingThread(long fromIndex, long toIndex) {
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }
    public abstract T getResult();
    public abstract void shutdown();
}
