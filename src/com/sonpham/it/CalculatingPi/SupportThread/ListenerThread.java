/*
*Classname: ListenerThread.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/
package com.sonpham.it.CalculatingPi.SupportThread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 *This class is used to stop threads by enter press.
 *
 */
public class ListenerThread{
	
	
	private boolean running = true;
	
	public boolean isRunning(){
		return running;
	}
	
	public void terminate(){
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		running = false; 
	}
}
