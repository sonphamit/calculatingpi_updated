/*
* Classname: CalculatorFactory.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.CalculatorFactoryPattern;

import com.sonpham.it.CalculatingPi.Calculator.BigCalculator;
import com.sonpham.it.CalculatingPi.Calculator.Calculator;
import com.sonpham.it.CalculatingPi.Calculator.SmallCalculator;

/*
 * The class is used to create object and refer to newly created object using a common Calculator interface
 * */
public class CalculatorFactory {
	
	/**
	 * @param decimalDegree this is necessary to get an object of Calculator.
	 * @return this returns object which we need calculator.
	 */
	public Calculator getCalculator(int decimalDegree){
		
		if(decimalDegree > 0 && decimalDegree < 17){
			return SmallCalculator.getInstance();
		}else{
			return BigCalculator.getInstance();
		}
	}

	
}
