/*
* Classname: Constant.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
*/

package com.sonpham.it.CalculatingPi.Common;

import java.math.BigDecimal;

public class Constant {

    public static final long N = 80000000;
    public static final int MAX_THREAD = 10;
    public static final long MAX_VALUE_PER_THREAD = N / MAX_THREAD;
    public static final BigDecimal ONE = new BigDecimal(1);
    public static final BigDecimal TWO = new BigDecimal(2);
    public static final BigDecimal N_ONE = new BigDecimal(-1);
    public static final BigDecimal FOUR = new BigDecimal(4);

}
