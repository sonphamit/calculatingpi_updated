/*
*Classname: MainApplication.
*
*@since August,9,2017.
*
*@author Minh Son Pham.
*
* The user can input the decimal place that that user wish to calculate the value of Pi to. 
* The program also support the ability to stop the calculating process and print out 
* the lastest result of the approximately.
*/

package com.sonpham.it.CalculatingPi.MainApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import com.sonpham.it.CalculatingPi.Calculator.Calculator;
import com.sonpham.it.CalculatingPi.CalculatorFactoryPattern.CalculatorFactory;

public class MainApplication {

	/**
	 * The main method is where the user can input the decimal place and calculate the value of Pi.
	 * @param args   
	 */
	public static void main(String[] args){
		
		/*The user inputs value of decimalDegree.*/ 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int decimalDegree;
		System.out.println("\nDecimalDegree = ");
		try {
			decimalDegree = Integer.parseInt(br.readLine());
			CalculatorFactory instanceCalculatorFactory = new CalculatorFactory();
			Calculator instanceCalculator = instanceCalculatorFactory.getCalculator(decimalDegree);
			instanceCalculator.calculate(decimalDegree);
			instanceCalculator.printResult();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
